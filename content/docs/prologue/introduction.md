---
title: "Introduction"
description: "Introduction Description."
lead: ""
date: 2018-01-21T15:21:01+02:00
lastmod: 2018-01-21T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "prologue"
weight: 10
toc: true
---

This site was created with the intention of curating documentation for AI Singapore engineers to navigate internal resources and tooling, with the hope that any relevant queries would be well-tended to.

## FAQ

+ _"How do I train models using AI Singapore's on-premise cluster?"_

  Engineers or apprentices can train models by submitting experiment jobs to [Polyaxon (Tekong cluster)](https://www.google.com). This can be done through [Kapitan Workspace](https://www.google.com) with the usage of [Polayxon's CLI](https://www.google.com).

+ _"Is it possible to leverage on multiple GPUs for distributed training?"_

  Multiple GPUs can be requested for experiment jobs to be submitted to Polyaxon. Guides for doing so can be found [here](https://www.google.com).

+ _"Is there any form of a blueprint or example for which an end-to-end workflow (data preparation to model training to on-prem deployment) can be carried out?"_

  Do refer to the following blog post for an example workflow: [`Basic End-to-end ML Workflow in AISG`](https://www.google.com)
