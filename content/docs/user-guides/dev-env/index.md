---
title: "Development Environment"
description: "Development Environment Description."
lead: "User guide for the development environments made available to engineers."
date: 2018-01-21T15:21:01+02:00
lastmod: 2018-01-21T15:21:01+02:00
draft: false
images: ["polyaxon-ui-prelogin.png"]
menu:
  docs:
    parent: "user guides"
weight: 10
toc: true
---

## Polyaxon Notebook

Polyaxon is a platform that enables AI practicioners with MLOps tooling and orchestration needs. Specifically, one is able to spin up development workspaces or experiment jobs as needed.

### Prerequisites

+ NUS VPN
+ Internet Browser (preferably Firefox or Chrome)
+ Polyaxon CLI (v0.5.6)
  + Installation: `pip install polyaxon==0.5.6`
+ Text editor of your choice

### Usage

1. Set AISG's Polyaxon server URL as endpoint:
```bash
$ polyaxon config set --host=polyaxon.okdapp.tekong.aisingapore.net --port=80 --use_https=False
```

2. Login to Polyaxon server:
```bash
$ polyaxon login -u <username>
```

3. Create a project (if you've yet to create one)
```bash
$ polyaxon project create --name=<project_name>
```

4. Initialise repository with relevant Polyaxon project:
```bash
$ polyaxon init <project_name>
$ polyaxon upload
$ polyaxon run -f specs/experiment.yml
```

_To be detailed with more instructions..._

## Kapitan Workspace

Kapitan Workspace is a platform with which you can spin up configured development environment within minutes. These development environments by default are equipped with various tooling options:
+ VSCode (in browser and can be connected to remotely)
+ Polyaxon CLI
+ DVC CLI
+ Kubernetes CLI

This project was developed by the Platforms apprentices from the 5th batch of AIAP, as part of the Kapitan Platform.

### Prerequisites

+ NUS VPN
+ Internet Browser (preferably Firefox or Chrome)
+ VSCode (optional)

### Usage

1. Head over to [Kapitan Workspace](https://kapitan.aisingapore.net/).

{{< img src="kapitan-workspace-landing.png" alt="Rectangle" caption="<em>Kapitan Workspace Landing Page</em>" class="border-0" >}}

2. Log in using your AI Singapore Google account.
3. Start the server.

### Additional Features

_To be detailed..._
