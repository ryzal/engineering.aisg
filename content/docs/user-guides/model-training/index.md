---
title: "Model Training"
description: "Model Training Description."
lead: "Model Training Lead."
date: 2018-01-21T15:21:01+02:00
lastmod: 2018-01-21T15:21:01+02:00
draft: false
images: ["polyaxon-ui-prelogin.png"]
menu:
  docs:
    parent: "user guides"
weight: 10
toc: true
---

## Polyaxon Setup

One can leverage on AI Singapore's on-premise Tekong cluster to train models with CPU or GPU (subject to availability). This can be done by submitting experiment jobs to Polyaxon, the platform which one can use to spin up various machine learning resources like notebooks, experiments and management tools. Before spinning up such resources, a project has to be created.

__Prerequisites:__
+ Logged in to NUS network/VPN
+ AI Singapore Azure account
+ Polyaxon CLI (this is pre-installed on Kapitan Workspaces)
+ A repository/project folder for which you would like to configure a Polyaxon project with

```bash
$ cd <project-repo>
$ polyaxon config set --host=polyaxon.okdapp.tekong.aisingapore.net --port=80 --use_https=False
$ polyaxon login -u <username>
$ polyaxon project create --name=<project_name> --description='My project.'
$ polyaxon init <project_name>
```

After the commands above, you should see the following files/directory created:

+ `.polyaxon` subdirectory
This subdirectory contains a `.polyaxonproject` file which tags this your project repository to the Polyaxon project. What this translates to is that a project repository can only be tagged to a single Polyaxon project at any time. This directory should be added to `.gitignore` so that other users of the repository won't be tagged to the same project.

+ `.polyaxonignore`
This file is akin to `.dockerignore`: the command `polyaxon upload` (to be used later) adopts a context from the working directory. Large or irrelevant files/directories should be specified in this file so that they will be ignored by Polyaxon.

You can inspect such projects through Polyaxon's dashboard UI hosted on the Tekong cluster using the following link:
> [http://polyaxon.okdapp.tekong.aisingapore.net](http://polyaxon.okdapp.tekong.aisingapore.net)

{{< img src="polyaxon-ui-prelogin.png" alt="Rectangle" caption="<em>Polyaxon UI Landing Page</em>" class="border-0" >}}

Now that we have set up a Polyaxon project, let's work towards submitting our first experiment job to Polyaxon.

## Single GPU Experiment

_To be detailed..._
