---
title: "Concepts"
description: "Concepts Description."
lead: ""
date: 2018-01-21T15:21:01+02:00
lastmod: 2018-01-21T15:21:01+02:00
draft: false
images: []
menu:
  docs:
    parent: "Polyaxon Docs"
weight: 300
toc: true
---

## Polyaxon Concepts

Polyaxon relies on a set of concepts to manage the experimentation process,
in this section we provide a high level introduction to these concepts,
with more details in pages dedicated to each concept.


### User

A `User` is the entity that creates projects, starts experiments, creates jobs and pipelines, manages teams and clusters.
A `User` has a set of permissions, and can be a normal user or a superuser.

> Please refer to the [users management section](/configuration/users-management/) for more details.

### Teams & Organizations

A `Team` provides a way to manage groups of users, their access roles, and resources quotas.

<blockquote class="warning"> This entity exists only on Polyaxon EE version</blockquote>

### Resources quotas

When a `quota` is attached to a user/team/project, the entity created, i.e. builds/jobs/experiments/notebooks, cannot exceed the parallelism and may not consume more
resources than the quota specification allows.

<blockquote class="warning"> This entity exists only on Polyaxon EE version</blockquote>

### Project

A `Project` in Polyaxon is very similar to a project in GitHub,
it aims at organizing your efforts to solve a specific problem.
A project consist of a name and a description, the code to execute, the data, and a polyaxonfile.yml.

> Please refer to the [projects section](/concepts/projects/) for more details.

### Experiment

An `Experiment` is the execution of your model with data and the provided parameters on the cluster.

An `Experiment Job` is the Kubernetes pod running on the cluster for a specific experiment,
if an experiment runs in a distributed way it will create multiple instances of `Experiment Job`.

> Please refer to the [experiments section](/concepts/experiments/) for more details.


### Experiment Group

An `Experiment Group` provide 2 interfaces:
  * An automatic and practical way to run a version of your model and data with different hyper parameters based on a hyperparameters search algorithm.
  * A selection of experiments to compare.

> Please refer to the [experiment groups - selection](/concepts/experiment-groups-selections/) for more details on how to create group selections

> Please refer to the [experiment groups - hyperparameters optimization](/concepts/experiment-groups-hyperparameters-optimization/) for more details on how to run hyperparameter search.


### Job

A `Job` is the execution of your code to do some data processing or any generic operation.

> Please refer to the [jobs section](/concepts/jobs/) for more details.

### Build Job

A `BuildJob` is the process of creating containers, Polyaxon provides different backends for creating containers.

> Please refer to the [build jobs section](/concepts/builds/) for more details.


### Tensorboard

A `Tensorboard` is a job running to visualize the metrics of an experiment,
the metrics of all experiments created during a hyperparameters-optimization group,
the metrics of all experiment in a selection group, or the experiments of a project.

> Please refer to the [tensorboards](/concepts/tensorboards/) for more details.

### Notebooks

A `Notebooks` is a job running project wide to provide a fast and easy way to explore data and start experiments.
Polyaxon provides different backends to start notebooks, or Jupyter Labs.

> Please refer to the [project notebooks§§](/concepts/notebooks/) for more details.


### Checkpointing, resuming and restarting experiments

Checkpointing is a very important concept in machine learning, it prevents losing progress.
It also provides the possibility to resume an experiment from a specific state.

Polyaxon provides some structure and organization regarding checkpointing and outputs saving.


> Please refer to the [save, resume & restart](/concepts/save-resume-restart/) for more details.
